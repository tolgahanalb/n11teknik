package com.n11.themoviedb;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.switch_screen)
    AppCompatImageView switchScreen;
    @BindView(R.id.back_button)
    AppCompatImageView backButton;

    boolean isGrid = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        Bundle bundle = new Bundle();
        bundle.putBoolean("isGrid", isGrid);
        ListFragment listFragment = new ListFragment();
        listFragment.setArguments(bundle);
        add(listFragment);


        setSupportActionBar(toolbar);
    }


    public void add(Fragment newFragment) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.main_fl, newFragment, newFragment.getClass().getName());
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @OnClick(R.id.switch_screen)
    public void switchOnclick(View view) {
        isGrid = !isGrid;

        if (isGrid)
            switchScreen.setImageDrawable(getResources().getDrawable(R.drawable.ic_one_row));
        else
            switchScreen.setImageDrawable(getResources().getDrawable(R.drawable.ic_two_row));

        Bundle bundle = new Bundle();
        bundle.putBoolean("isGrid", isGrid);
        ListFragment listFragment = new ListFragment();
        listFragment.setArguments(bundle);
        add(listFragment);
    }

    @OnClick(R.id.back_button)
    public void backOnClick(View view) {
        onBackPressed();
    }


}
