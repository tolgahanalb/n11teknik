package com.n11.themoviedb.service;

import com.n11.themoviedb.service.model.detail.MovieDetail;
import com.n11.themoviedb.service.model.list.DiscoverMovie;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("discover/movie")
    Call<DiscoverMovie> discoverMovie(@Query("api_key") String api_key,@Query("sort_by") String sort_by,@Query("page") Integer page);

    @GET("movie/{movie_id}")
    Call<MovieDetail> movieDetail(@Path("movie_id") Integer movie_id, @Query("api_key") String api_key);
}
