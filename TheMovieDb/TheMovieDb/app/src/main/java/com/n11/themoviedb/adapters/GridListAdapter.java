package com.n11.themoviedb.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.n11.themoviedb.R;
import com.n11.themoviedb.service.model.list.Result;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GridListAdapter extends BaseAdapter {

    private ArrayList<Result> data = null;
    private Context context = null;
    private LayoutInflater inflater;

    class ViewHolder {

        @BindView(R.id.movie_iv)
        AppCompatImageView movieIv;
        @BindView(R.id.movie_relase_date_tv)
        AppCompatTextView movieRelaseDateTv;
        @BindView(R.id.movie_title_tv)
        AppCompatTextView movieTitleTv;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }

    public GridListAdapter(Context context, ArrayList<Result> data) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return (data == null) ? 0 : data.size();
    }

    @Override
    public Object getItem(int position) {
        return (data == null) ? null : data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;

        if (view == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_grid, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.movieTitleTv.setText(data.get(position).getTitle() != null ? data.get(position).getTitle() : "-");
        holder.movieRelaseDateTv.setText(data.get(position).getReleaseDate() != null ? data.get(position).getReleaseDate() : "-");

        if (data.get(position).getPosterPath() != null) {
            Picasso.get()
                    .load("https://image.tmdb.org/t/p/w300_and_h450_bestv2/"+data.get(position).getPosterPath())
                    .error(R.mipmap.ic_launcher)
                    .fit()
                    .centerCrop()
                    .into(holder.movieIv);
        } else
            holder.movieIv.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_launcher));
        return view;
    }

    public void refreshData(ArrayList<Result> newData) {
        this.data = newData;
        this.notifyDataSetChanged();
    }
}
