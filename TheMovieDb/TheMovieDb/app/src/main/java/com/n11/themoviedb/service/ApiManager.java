package com.n11.themoviedb.service;

import com.n11.themoviedb.service.model.detail.MovieDetail;
import com.n11.themoviedb.service.model.list.DiscoverMovie;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {


    private static final String APIURL = "https://api.themoviedb.org/3/";
    private static final String APIKEY = "92debed7d8182a36f3d20730dc382a46";
    private static ApiManager instance = new ApiManager();
    private static ApiInterface apiInterface;

    public static ApiManager getInstance() {
        if (instance == null) {
            instance = new ApiManager();
        }
        if (apiInterface == null) {
            createApiInterface();
        }
        return instance;
    }


    private static void createApiInterface() {


        Retrofit retrofit = new Retrofit.Builder().baseUrl(APIURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterface = retrofit.create(ApiInterface.class);
    }


    public Call<DiscoverMovie> discoverMovie(Integer page){
        return apiInterface.discoverMovie(APIKEY,"popularity.desc",page);
    }

    public Call<MovieDetail> movieDetail(Integer movieId){
        return apiInterface.movieDetail(movieId,APIKEY);
    }


}
