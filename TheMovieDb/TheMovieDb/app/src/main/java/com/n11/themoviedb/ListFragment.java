package com.n11.themoviedb;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.n11.themoviedb.adapters.GridListAdapter;
import com.n11.themoviedb.adapters.ListAdapter;
import com.n11.themoviedb.service.ApiManager;
import com.n11.themoviedb.service.model.list.DiscoverMovie;
import com.n11.themoviedb.service.model.list.Result;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListFragment extends Fragment implements AbsListView.OnScrollListener {

    @BindView(R.id.list_view)
    ListView listView;
    @BindView(R.id.grid_view)
    GridView gridView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private int page = 1;
    private int total = 20;
    private boolean isLoading = false;

    ArrayList<Result> data = null;

    private ListAdapter mPLAdapter;
    private GridListAdapter gridLAdapter;

    private Boolean isGrid;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isGrid = this.getArguments().getBoolean("isGrid");
        ((MainActivity) getActivity()).switchScreen.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).backButton.setVisibility(View.GONE);

        View roowView = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, roowView);
        return roowView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        data = new ArrayList<Result>();
        mPLAdapter = new ListAdapter(getActivity(), data);
        gridLAdapter = new GridListAdapter(getActivity(), data);

        if (!isGrid) {
            listView.setVisibility(View.VISIBLE);
            listView.setAdapter(mPLAdapter);
            listView.setOnScrollListener(this);
            gridView.setVisibility(View.GONE);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Result result = (Result) mPLAdapter.getItem(position);


                    openFragment(result);
                }
            });
        } else {
            gridView.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            gridView.setAdapter(gridLAdapter);
            gridView.setOnScrollListener(this);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Result result = (Result) mPLAdapter.getItem(position);

                    openFragment(result);
                }
            });
        }

        nextPage();

    }

    private void openFragment(Result result) {
        Bundle bundle = new Bundle();
        bundle.putInt("Id", result.getId());
        bundle.putString("Title", result.getTitle());
        bundle.putString("Overview", result.getOverview());
        bundle.putString("Image", result.getPosterPath());
        bundle.putString("RealaseDate", result.getReleaseDate());
        DetailFragment detailFragment = new DetailFragment();
        detailFragment.setArguments(bundle);

        ((MainActivity) (getActivity())).add(detailFragment);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        if (!isGrid) {
            if (totalItemCount > 0 && listView != null) {
                int lastPosition = listView.getLastVisiblePosition();
                if (lastPosition + 1 == totalItemCount && data.size() <= total) {
                    nextPage();
                    isLoading = true;
                }
            }
        } else {
            if (totalItemCount > 0 && gridView != null) {
                int lastPosition = gridView.getLastVisiblePosition();
                if (lastPosition + 1 == totalItemCount && data.size() <= total) {
                    nextPage();
                    isLoading = true;
                }
            }
        }
    }

    private void nextPage() {
        if (!isLoading) {
            if (page <= 5) {
                progressBar.setVisibility(View.VISIBLE);
                ApiManager.getInstance().discoverMovie(page).enqueue(new Callback<DiscoverMovie>() {
                    @Override
                    public void onResponse(Call<DiscoverMovie> call, Response<DiscoverMovie> response) {
                        progressBar.setVisibility(View.GONE);
                        isLoading = false;
                        data.addAll(response.body().getResults());
                        mPLAdapter.refreshData(data);
                        gridLAdapter.refreshData(data);
                        page++;
                        total = total + 20;
                    }

                    @Override
                    public void onFailure(Call<DiscoverMovie> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }


    }

}
