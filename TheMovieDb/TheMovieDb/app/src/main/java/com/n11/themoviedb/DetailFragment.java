package com.n11.themoviedb;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.n11.themoviedb.service.ApiManager;
import com.n11.themoviedb.service.model.detail.Genre;
import com.n11.themoviedb.service.model.detail.MovieDetail;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailFragment extends Fragment {


    @BindView(R.id.movie_detail_iv)
    AppCompatImageView movieDetailIv;
    @BindView(R.id.movie_detail_relase_tv)
    AppCompatTextView movieDetailRelaseTv;
    @BindView(R.id.movie_detail_overview_tv)
    AppCompatTextView movieDetailOverviewTv;
    @BindView(R.id.movie_detail_genre_tv)
    AppCompatTextView movieDetailGenreTv;
    @BindView(R.id.movie_detail_budget_tv)
    AppCompatTextView movieDetailBudgetwTv;
    @BindView(R.id.movie_detail_reveneue_tv)
    AppCompatTextView movieDetailRevenueTv;
    @BindView(R.id.movie_detail_average_tv)
    AppCompatTextView movieDetailAverageTv;
    @BindView(R.id.progress_rl)
    RelativeLayout progressRl;
    @BindView(R.id.data_ll)
    LinearLayout dataLl;
    @BindView(R.id.error_rl)
    RelativeLayout errorRl;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).switchScreen.setVisibility(View.GONE);
        ((MainActivity) getActivity()).backButton.setVisibility(View.VISIBLE);


        View roowView = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this, roowView);
        return roowView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Integer id = this.getArguments().getInt("Id", 0);
        final String overview = this.getArguments().getString("Overview");
        final String image = this.getArguments().getString("Image");
        final String title = this.getArguments().getString("Title");
        final String realaseDate = this.getArguments().getString("RealaseDate");


        ApiManager.getInstance().movieDetail(id).enqueue(new Callback<MovieDetail>() {
            @Override
            public void onResponse(Call<MovieDetail> call, Response<MovieDetail> response) {
                progressRl.setVisibility(View.GONE);
                dataLl.setVisibility(View.VISIBLE);
                MovieDetail movieDetail = response.body();

                List<Genre> genres = movieDetail.getGenres();
                Integer budget = movieDetail.getBudget();
                Integer revenue = movieDetail.getRevenue();
                Double voteAverage = movieDetail.getVoteAverage();


                Picasso.get()
                        .load("https://image.tmdb.org/t/p/w300_and_h450_bestv2/" + image)
                        .fit().centerCrop()
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(movieDetailIv);

                movieDetailRelaseTv.setText(realaseDate != null ? realaseDate : "-");
                movieDetailOverviewTv.setText(overview != null ? overview : "-");
                movieDetailRevenueTv.setText(revenue != null ? revenue.toString() : "-");
                movieDetailBudgetwTv.setText(budget != null ? budget.toString() : "-");

                movieDetailRelaseTv.setText(realaseDate != null ? realaseDate : "-");

                movieDetailAverageTv.setText(voteAverage != null ? voteAverage.toString():"-");

                StringBuilder s = new StringBuilder();
                for (Genre genre : genres) {
                    s.append(genre.getName() + "    ");
                }
                movieDetailGenreTv.setText(s);
            }

            @Override
            public void onFailure(Call<MovieDetail> call, Throwable t) {
                progressRl.setVisibility(View.GONE);
                errorRl.setVisibility(View.VISIBLE);
                t.printStackTrace();
            }
        });
    }
}
